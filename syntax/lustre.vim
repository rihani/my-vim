if exists("b:current_syntax")
	  finish
endif
syn clear

syn match lusStatement "=\|;"
syn match default '[a-zA-Z_][a-zA-Z0-9_]*'
syn keyword Type int real bool
syn keyword Control if else then
syn keyword Node node let var returns
syn keyword lusInclude include
syn match   Node "tel."
syn match   Node "tel"
syn keyword Commands pre current when and or not
syn match Commands "->"
syn keyword Constant true false
syn match Constant "[-]\d*[\.]\d"
syn match Constant "\d*[\.]\d*"

syn region Comment start="--" end="$"
syn region Comment start="(\*" end="\*)" excludenl
syn region Comment start="/\*" end="\*/" excludenl

let b:current_syntax = "lustre"
hi Node		term=NONE cterm=bold  ctermfg=1  gui=bold  guifg=#ffff60
hi link lusStatement Statement 
hi link lusInclude PreProc
"term=NONE  ctermfg=14  gui=NONE  guifg=#ffff60
hi Commands term=bold cterm=bold ctermfg=3 guifg=DarkCyan 	gui=bold 

hi link Control Commands 

